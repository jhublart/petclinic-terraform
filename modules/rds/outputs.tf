output "vet_db_endpoint" {
  description = "Endpoint for the vet-db instance"
  value       = aws_db_instance.db[0].endpoint
}

output "customer_db_endpoint" {
  description = "Endpoint for the customer-db instance"
  value       = aws_db_instance.db[1].endpoint
}

output "visit_db_endpoint" {
  description = "Endpoint for the visit-db instance"
  value       = aws_db_instance.db[2].endpoint
}
