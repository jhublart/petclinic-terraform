variable "velero_bucket_name" {
  description = "Name of the S3 Bucket used to backup Cluster with Velero."
  type        = string
}